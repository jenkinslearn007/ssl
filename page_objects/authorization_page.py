from page_objects.base_page import BasePage

EMAIL_INPUT = {'css': 'input[placeholder="Email"]'}
PASSWORD_INPUT = {'css': 'input[placeholder="Enter password"]'}
TITLE = {'css': '.page-title'}
LOGIN_BUTTON = {'css': 'button[type="submit"]'}
EMAIL_FIELD_ALERT_TEXT = {'css': '.form-group.email > div[class="left-tooltip-box"]'}
PASSWORD_FIELD_ALERT_TEXT = {'css': 'div[class="form-group"] > div[class="left-tooltip-box"]'}


class AuthorizationPage(BasePage):

    def set_email(self, email):
        self._set_text(EMAIL_INPUT, text=email)
        return self

    def set_password(self, password):
        self._set_text(PASSWORD_INPUT, text=password)
        return self

    def get_page_title(self):
        return self._get_text(TITLE)

    def click_login_button(self):
        self._click(LOGIN_BUTTON)

    def get_email_field_alert_text(self):
        return self._get_text(EMAIL_FIELD_ALERT_TEXT)

    def get_password_field_alert_text(self):
        return self._get_text(PASSWORD_FIELD_ALERT_TEXT)

from page_objects.base_page import BasePage

ALERT = {'css': '.noty_message.message > span'}


class AlertPage(BasePage):

    def get_alert_message_text(self):
        self._wait_for_visible(ALERT)
        return self._get_text(ALERT)

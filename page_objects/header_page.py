from page_objects.base_page import BasePage

LOGIN_BUTTON = {'css': '.ssls-header-add-nav > button'}


class HeaderPage(BasePage):

    def click_login_button(self):
        self._click(LOGIN_BUTTON, index=1)

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def __element(self, selector, index):
        by = None
        if 'css' in selector.keys():
            by = By.CSS_SELECTOR
            selector = selector['css']
        elif 'xpath' in selector.keys():
            by = By.XPATH
            selector = selector['xpath']
        return self.driver.find_elements(by, selector)[index - 1]

    def _click(self, selector, index=1):
        self.__element(selector, index).click()

    def _set_text(self, selector, text, index=1):
        _elem = self.__element(selector, index)
        _elem.clear()
        _elem.send_keys(text)

    def _get_text(self, selector, index=1):
        return self.__element(selector, index).text

    def _wait_for_visible(self, selector, index=0, wait=3):
        return WebDriverWait(self.driver, wait).until(EC.visibility_of(self.__element(selector, index)))

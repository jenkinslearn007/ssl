from pathlib import Path

import pytest
from selenium import webdriver


def pytest_addoption(parser):
    parser.addoption("--browser", "-B", action="store", default="chrome")


@pytest.fixture(scope='session')
def browser(request):
    browser = request.config.getoption("--browser")
    if browser == "chrome":
        driver = webdriver.Chrome(executable_path=Path(__file__).parent / "web_drivers/chromedriver.exe")
    elif browser == "firefox":
        driver = webdriver.Firefox(executable_path=Path(__file__).parent / "web_drivers/geckodriver.exe")
    else:
        raise Exception(f'{request.param} is not supported')
    driver.implicitly_wait(20)
    driver.delete_all_cookies()
    driver.maximize_window()
    driver.get("https://www.ssls.com/")
    request.addfinalizer(driver.quit)
    return driver


# @pytest.fixture(scope='session')
# def main_page(browser):
#     browser.get("https://www.ssls.com/")
#     return browser

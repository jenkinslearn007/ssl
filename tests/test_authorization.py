from page_objects import HeaderPage, AuthorizationPage, AlertPage

HOME_URL = 'https://www.ssls.com/'


def test_home_page_is_opened(browser):
    page = browser

    assert page.current_url == HOME_URL


def test_authorization_page_is_opened(browser):
    page = browser

    HeaderPage(page).click_login_button()

    assert AuthorizationPage(page).get_page_title() == 'Authorization'


def test_not_registered_user(browser):
    page = browser

    AuthorizationPage(page) \
        .set_email(email='ssls.not_registered_user@gmail.com') \
        .set_password(password=123456) \
        .click_login_button()

    assert AlertPage(page).get_alert_message_text() == 'Uh oh! Email or password is incorrect'


def test_invalid_email(browser):
    page = browser

    AuthorizationPage(page) \
        .set_email(email='test@@test.com') \
        .set_password(password=123456) \
        .click_login_button()

    assert AuthorizationPage(page).get_email_field_alert_text() == 'Uh oh! This\nisn’t an email'


def test_empty_fields(browser):
    page = browser

    AuthorizationPage(page) \
        .set_email(email='') \
        .set_password(password='') \
        .click_login_button()

    assert AuthorizationPage(page).get_email_field_alert_text() == 'Oops, please\nenter your email'
    assert AuthorizationPage(page).get_password_field_alert_text() == 'Looks like you’ve\nmissed this one'


